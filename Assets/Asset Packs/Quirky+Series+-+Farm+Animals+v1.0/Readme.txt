Thanks for downloading our asset!
If you think we'd benefit from a review, please do - so we could improve!

Visit our website: www.omabuarts.com
Preview the model in 3D/AR/VR: https://skfb.ly/6yVE9

Animals:
 Chick
 Cow
 Duck
 Sheep

Features:
 8x1 texture.
 9 animations.

Animations:
 1. Idle
 2. Walk
 3. Run
 4. Bounce
 5. Swim/Fly
 6. Roll
 7. Spin
 8. Munch/Peck
 9. Clicked

Preview the model in 3D/AR/VR: https://skfb.ly/6yVE9

Enquiries?
Leave us an email at omabuarts@gmail.com

Visit our website: www.omabuarts.com

Follow us:
Discord  : https://discord.gg/WaezE8B
Facebook : https://www.facebook.com/Polydactyl-440193949513560
Sketchfab: https://sketchfab.com/omabuarts