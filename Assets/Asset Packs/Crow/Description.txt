A low poly stylized _ model suitable for your project needs.
<br>
<br>Features:
<br>894 tris.
<br>7 Hand-painted 1024x1024 texture.
<br>12 animations.
<br>
<br><a href="https://goo.gl/PHFKFg">Web Player Demo</a>
<br><a href="https://sketchfab.com/omabuarts">Demo on our Sketchfab</a>
<br>
<br>Available Animations:
<br>1. Idle - 4 animations
<br>2. Eat
<br>3. Drink
<br>4. Fly - 2 animations
<br>5. Attack - 2 animations
<br>6. Hit
<br>7. Death
<br>
<br>
<br>Enquiries?
<br>Leave us an email at omabuarts@gmail.com
<br>
<br>Follow us:
<br><a href="https://www.facebook.com/Polydactyl-440193949513560">Polydactyl on Facebook</a>
<br>Sketchfab: <a href="https://sketchfab.com/omabuarts">https://sketchfab.com/omabuartsSketchfab</a>

low poly toon bird animal animated stylized

DEMO LINKS:
Birds - https://goo.gl/PHFKFg

SKFB LINKS:
Seagull - https://skfb.ly/QqEM

UAS LINKS:
Birds - http://u3d.as/wrT
Toucan - http://u3d.as/t86
Seagull - http://u3d.as/wrR
Parrots - http://u3d.as/wsj
