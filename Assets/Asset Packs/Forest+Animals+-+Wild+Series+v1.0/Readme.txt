Thanks for downloading our asset!
If you think we'd benefit from a review, please do - so we could improve!

Preview the models in 3D/AR/VR: https://sketchfab.com/omabuarts/collections/wild-series

We have included:
 1. Animal_HD.fbx - High Detail mesh of the model
 2. Animal_MD.fbx - Medium Detail mesh of the model
 3. Animal_SD.fbx - Small Detail mesh of the model
 3. Animal_TD.fbx - Tiny Detail mesh of the model
 4. Animal_Anim.fbx - Animation file + HD mesh
 5. Tex_Animal.jpg - Texture file

Animals:
 Bear
 Deer
 Fox
 Red Panda
 Wolf

Features:
 4 LoDs
 64x64 texture.
 8 extended animations.

Animations:
 1. Idle + Sit
 2. Walk + [side & backward]
 3. Run + [side]
 4. Jump
 5. Turn
 6. Eat
 7. Sleep

Preview the models in 3D/AR/VR: https://sketchfab.com/omabuarts/collections/wild-series

Enquiries?
Leave us an email at omabuarts@gmail.com

Follow us:
Discord  : https://discord.gg/WaezE8B
Facebook : https://www.facebook.com/Polydactyl-440193949513560
Sketchfab: https://sketchfab.com/omabuarts