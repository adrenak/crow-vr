﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class Splash : MonoBehaviour {
	public VideoPlayer player;

	void Start() {
		player.loopPointReached += Player_loopPointReached;
	}

	private void Player_loopPointReached(VideoPlayer source) {
		SceneManager.LoadScene("Gameplay");
	}
}
