﻿using UnityEngine;
using Adrenak.UniGenVR;

public class DeviceController : MonoBehaviour {
	public enum InputMode {
		Head,
		Hand,
		Both
	}

	[SerializeField] Interactor m_HeadInteractor;
    [SerializeField] Interactor m_RightHandInteractor;
    [SerializeField] Interactor m_LeftHandInteractor;

	public void ActivateInteractors(InputMode mode) {
		switch (mode) {
			case InputMode.Head:
				m_HeadInteractor.enabled = true;
				m_RightHandInteractor.enabled = false;
				m_LeftHandInteractor.enabled = false;
				break;
			case InputMode.Hand:
				m_HeadInteractor.enabled = false;
				m_RightHandInteractor.enabled = true;
				m_LeftHandInteractor.enabled = true;
				break;
			case InputMode.Both:
				m_HeadInteractor.enabled = true;
				m_RightHandInteractor.enabled = true;
				m_LeftHandInteractor.enabled = true;
				break;
		}
	}
}
