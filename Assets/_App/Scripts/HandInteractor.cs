﻿using UnityEngine;
using UnityEngine.UI;
using Adrenak.UniGenVR;

public class HandInteractor : MonoBehaviour {
    [SerializeField] Interactor m_Interactor;

    private void Update() {
        bool isTrigger = OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger);

        SetIsTrigger(isTrigger);
    }

    private void SetIsTrigger(bool isOn) {
        m_Interactor.isRaycasting = isOn;
    }
}