﻿using UnityEngine;
using System;
using Adrenak.Unex;
using UnityEngine.Events;
using UnityEngine.UI;

public class VRKeyboard : MonoBehaviour {
    public event Action<string> OnKeystroke;
    public event Action OnBackstroke;
    public event Action<string> OnTextChanged;
    public event Action<string> OnSubmit;
    public UnityEvent onClose;
    public UnityEvent onOpen;

    [SerializeField] Text m_InputText;

    string m_Input = string.Empty;
    CanvasGroup m_CanvasGroup;

    void Awake() {
        m_CanvasGroup = GetComponent<CanvasGroup>();
    }

    public string Input {
        get { return m_Input; }
        private set {
            m_Input = value;
            m_InputText.text = m_Input;
            OnTextChanged.TryInvoke(m_Input);
        }
    }

    public void ClearAll() {
        Input = string.Empty;
    }

    public void Press(string key) {
        Input += key;
        OnKeystroke.TryInvoke(key);
    }

    public void Clear() {
        OnBackstroke.TryInvoke();
        if (Input.Length > 0)
            Input = Input.Substring(0, Input.Length - 1);
    }

    public void Space() {
        Input += " ";
    }

    public void Submit() {
        OnSubmit.TryInvoke(Input);
    }

    public void Close() {
        Input = string.Empty;
		m_CanvasGroup.alpha = 0;
		m_CanvasGroup.interactable = false;
		m_CanvasGroup.blocksRaycasts = false;
		onClose.Invoke();
    }

    public void Open() {
		m_CanvasGroup.alpha = 1;
		m_CanvasGroup.interactable = true;
		m_CanvasGroup.blocksRaycasts = true;
		onOpen.Invoke();
    }

    public void Toggle() {
        gameObject.SetActive(!gameObject.activeSelf);
    }
}