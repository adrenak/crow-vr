﻿using UnityEngine;
using Adrenak.Shiain;
using UnityEngine.UI;

public class ScoreWidget : PopulationWidget {
    [SerializeField] Text m_NameText;
    [SerializeField] Text m_ScoreText;

    public override void CleanUp() {
        m_NameText.text = "";
        m_ScoreText.text = "";
    }

    public override void Init(object data) {
        var uiData = (UserScoreEntry)data;
        var name = uiData.Name;
        var score = uiData.Score;

        m_NameText.text = name;
        m_ScoreText.text = score.ToString();
    }
}
