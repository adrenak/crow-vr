﻿using UnityEngine;

public class AppUI : MonoBehaviour {
	// These events are of interest to the AppManager
	[SerializeField] AGameplayCanvas m_GameplayCanvas;
	[SerializeField] AMenuCanvas m_MenuCanvas;
	[SerializeField] AScoreCanvas m_ScoreCanvas;
    
    // Called by the GameManager when the app starts
	public void ShowMenu(string status = null) {
		m_ScoreCanvas.DisableCanvas();
		m_GameplayCanvas.DisableCanvas();
		m_MenuCanvas.EnableCanvas();

		if (status == null)
			status = "Select a difficulty level to start!";
		m_MenuCanvas.SetStatus(status);
	}

	// Called by GameManager when the game is starting
	public void ShowGameplay(int totalRockCount, bool hasTimeLimit) {
		m_MenuCanvas.DisableCanvas();
		m_ScoreCanvas.DisableCanvas();
		m_GameplayCanvas.EnableCanvas();
		m_GameplayCanvas.OnGameStart(totalRockCount, hasTimeLimit);
    }
	
	// Called by GameManager when the game is lost because of time running out
	public void ShowLoss() {
		m_GameplayCanvas.DisableCanvas();
		m_ScoreCanvas.DisableCanvas();
		m_MenuCanvas.EnableCanvas();
		m_MenuCanvas.SetStatus("Time over!");
	}

	// Called by GameManager when the game is won by the player
	public void ShowVictory(Gameplay.Level level, int points) {
		m_GameplayCanvas.DisableCanvas();
		m_MenuCanvas.DisableCanvas();
		m_ScoreCanvas.EnableCanvas();
		m_ScoreCanvas.Display(points);
	}
}