﻿using UnityEngine;
using System.Collections.Generic;

public abstract class AScoreCanvas : ACanvas {
	[SerializeField] protected ScoreController m_Controller;

	public abstract void Display(int points);
	public abstract void PopoulateEntries(List<UserScoreEntry> entries);
	public abstract void SetStatus(string status);
}
