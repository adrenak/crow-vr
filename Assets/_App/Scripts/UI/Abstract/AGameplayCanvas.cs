﻿using UnityEngine;

public abstract class AGameplayCanvas : ACanvas {
	[SerializeField] protected GameplayController m_Controller;

	public abstract void OnGameStart(int totalRockCount, bool hasTimeLimit);
	public abstract void SetStatus(string status);
	public abstract void SetRockCount(int count, int total);
}
