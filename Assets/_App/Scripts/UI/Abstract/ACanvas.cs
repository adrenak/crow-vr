﻿using System;
using Adrenak.UniGenVR;
using UnityEngine;
using UnityEngine.UI;

public abstract class ACanvas : MonoBehaviour {
	public event Action OnEnableCanvas;
	public event Action OnDisableCanvas;
	Canvas m_Canvas;
    CanvasScaler m_Scaler;
	
	protected void AwakeBase() {
		m_Canvas = GetComponent<Canvas>();
        m_Scaler = GetComponent<CanvasScaler>();
	}

	public void EnableCanvas() {
		m_Canvas.enabled = true;
        m_Scaler.enabled = true;
        OnEnableCanvas?.Invoke();
	}

	public void DisableCanvas() {
		m_Canvas.enabled = false;
        m_Scaler.enabled = false;
		OnDisableCanvas?.Invoke();
	}
}
