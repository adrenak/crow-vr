﻿using UnityEngine;

public abstract class AMenuCanvas : ACanvas{
	[SerializeField] protected MenuController m_Controller;

	public abstract void SetStatus(string status);
}
