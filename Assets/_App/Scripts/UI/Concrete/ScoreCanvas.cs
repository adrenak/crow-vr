﻿using UnityEngine;
using Adrenak.Shiain;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class ScoreCanvas : AScoreCanvas {
	[SerializeField] Text m_Message;

	[SerializeField] SimplePopulator m_Populator;
	[SerializeField] PopulationWidget m_Widget;
	[SerializeField] VRKeyboard m_Keyboard;
	[SerializeField] Button m_SubmitButton;
	[SerializeField] Button m_CancelButton;

	void Awake() {
		AwakeBase();
		m_SubmitButton.onClick.AddListener(() => Submit());
		m_CancelButton.onClick.AddListener(() => Close());
	}

	public override void Display(int points) {
		SetStatus("Score: " + points + ". You can submit your score by entering your name!");
		m_Keyboard.Open();
	}

	public override void PopoulateEntries(List<UserScoreEntry> entries) {
        m_Populator.Clear();
        m_Populator.SetWidgetPrefab(m_Widget);
		m_Populator.Populate(entries.ToArray(), 5);
	}

	public override void SetStatus(string status) {
		m_Message.text = status;
	}

	public void Close() {
        DisableCanvas();
		m_Controller.OnCanvasClose();
	}

	public void Submit() {
		var input = m_Keyboard.Input;
		m_Controller.OnSubmit(input);
	}
}

