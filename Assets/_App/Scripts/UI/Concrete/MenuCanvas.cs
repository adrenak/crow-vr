﻿using Adrenak.UniGenVR;
using UnityEngine;
using UnityEngine.UI;

public class MenuCanvas : AMenuCanvas {
	[SerializeField] Text m_StatusText;

	[Header("Game Difficulty Levels")]
	[SerializeField] Interactable m_EasyGuage;
	[SerializeField] Interactable m_MediumGuage;
	[SerializeField] Interactable m_HardGuage;

	private void Awake() {
		AwakeBase();

        OnEnableCanvas += () => {
            m_EasyGuage.gameObject.SetActive(true);
            m_MediumGuage.gameObject.SetActive(true);
            m_HardGuage.gameObject.SetActive(true);
        };

        OnDisableCanvas += () => {
            m_EasyGuage.gameObject.SetActive(false);
            m_MediumGuage.gameObject.SetActive(false);
            m_HardGuage.gameObject.SetActive(false);
        };

		m_EasyGuage.OnGazedEvent += _ => m_Controller.SelectLevel(Gameplay.Level.Easy);
		m_MediumGuage.OnGazedEvent += _ => m_Controller.SelectLevel(Gameplay.Level.Medium);
		m_HardGuage.OnGazedEvent += _ => m_Controller.SelectLevel(Gameplay.Level.Hard);
	}

	public override void SetStatus(string status) {
		m_StatusText.text = status;
	}
}
