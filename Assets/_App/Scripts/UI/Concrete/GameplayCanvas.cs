﻿using System;
using Adrenak.UniGenVR;
using UnityEngine;
using UnityEngine.UI;

public class GameplayCanvas : AGameplayCanvas {
	public event Action OnGameStopped;

	[SerializeField] GameplayController m_Gameplay;
	[SerializeField] Text m_RockCountText;
	[SerializeField] Text m_StatusText;
	[SerializeField] Timer m_Timer;
	[SerializeField] Interactable m_StopGuage;

	void Awake() {
		AwakeBase();		
	}

	void Start() {
		m_StopGuage.OnGazedEvent += _ => m_Controller.StopGame();
	}

	public override void OnGameStart(int totalRockCount, bool hasTimeLimit) {
		SetStatus("Put all the rocks in the bottle!");
		SetRockCount(0, totalRockCount);
		m_StopGuage.gameObject.SetActive(true);

		if (hasTimeLimit)
			m_Timer.Show();
		else
			m_Timer.Hide();
	}

	public override void SetStatus(string status) {
		m_StatusText.text = status;
	}

	public override void SetRockCount(int count, int total) {
		m_RockCountText.text = count + "/" + total;
	}
}
