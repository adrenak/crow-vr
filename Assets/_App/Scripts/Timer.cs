﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
    public Action OnTimeOver;
    [SerializeField] Text display;

	CanvasGroup m_Group;
	int m_TotalTime;
	int m_TimeLeft = 120;
    bool m_IsRunning;

	private void Awake() {
		m_Group = GetComponent<CanvasGroup>();
	}

	public void Run(int totalTime) {
		if (totalTime < 0) return;

        m_IsRunning = true;
        m_TotalTime = totalTime;
		m_TimeLeft = m_TotalTime;
		StartCoroutine(Tick());
    }

	IEnumerator Tick() {
		while (m_IsRunning) {
			UpdateUI();
			yield return new WaitForSeconds(1);
			m_TimeLeft--;
			if (m_TimeLeft <= 0) {
				OnTimeOver?.Invoke();
				yield break;
			}
		}
	}

	public int GetTimeElapsed() {
		return m_TotalTime - m_TimeLeft;
	}

    public int GetTimeLeft() {
        return m_TimeLeft;
    }

    public void Pause() {
        m_IsRunning = false;
    }

	public void Stop() {
		Pause();
		m_TimeLeft = 0;
		m_TotalTime = 0;
	}

    public void UpdateUI() {
        int minutes = m_TimeLeft / 60;
		int seconds = m_TimeLeft % 60;

        display.text = minutes.ToString("00") + ":" + seconds.ToString("00");
    }

    public void Hide() {
		m_Group.alpha = 0;
    }

    public void Show() {
		m_Group.alpha = 1;
    }
}
