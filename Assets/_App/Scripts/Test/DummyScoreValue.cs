﻿using UnityEngine;
using System.Collections;

public class DummyScoreValue : MonoBehaviour {

    private IEnumerator Start() {
        yield return new WaitForSeconds(2f);

        Backend.Submit("john", Gameplay.Level.Easy, 20);
        Backend.Submit("emily", Gameplay.Level.Easy, 15);
        Backend.Submit("dean", Gameplay.Level.Easy, 10);

        Backend.Submit("john", Gameplay.Level.Medium, 20);
        Backend.Submit("emily", Gameplay.Level.Medium, 15);
        Backend.Submit("dean", Gameplay.Level.Medium, 10);

        Backend.Submit("john", Gameplay.Level.Hard, 20);
        Backend.Submit("emily", Gameplay.Level.Hard, 15);
        Backend.Submit("dean", Gameplay.Level.Hard, 10);
    }
}