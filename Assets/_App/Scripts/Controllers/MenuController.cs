﻿using System;
using UnityEngine;

public class MenuController : MonoBehaviour {
	public event Action<Gameplay.Level> OnLevelSelect;

	[SerializeField] AMenuCanvas m_Canvas;

	public void OnApplicationStart() { }

	public void SelectLevel(Gameplay.Level level) {
		OnLevelSelect?.Invoke(level);
	}
}
