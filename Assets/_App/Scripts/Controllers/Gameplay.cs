﻿public class Gameplay {
	public enum Level {
		Easy,
		Medium,
		Hard
	}

	public class Options {
		public int rockCount;
		public int timeLimit;
		public float maxPitch;
		public float minPitch;
		public float maxYaw;
		public float minYaw;

		public bool HasTimeLimit {
			get { return timeLimit > 0; }
		}

		public static Gameplay.Options GetPreset(Gameplay.Level level) {
			var options = new Gameplay.Options();
			switch (level) {
				case Gameplay.Level.Easy:
					options.rockCount = 5;

					options.timeLimit = 60;

					options.maxPitch = 30;
					options.maxYaw = 30;
					options.minYaw = -30;
					break;
				case Gameplay.Level.Medium:
					options.rockCount = 7;

					options.timeLimit = 90;

					options.maxPitch = 55;
					options.maxYaw = 55;
					options.minYaw = -55;
					break;
				case Gameplay.Level.Hard:
					options.rockCount = 9;

					options.timeLimit = 120;

					options.maxPitch = 80;
					options.maxYaw = 80;
					options.minYaw = -80;
					break;
			}
			//options.timeLimit = -1;
			options.minPitch = -20;
			return options;
		}
	}

}
