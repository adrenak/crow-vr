﻿using System;
using UnityEngine;
using System.Linq;

public class ScoreController : MonoBehaviour {
	public event Action<string> OnSubmitted;
	public event Action OnCanvasClosed;

	[SerializeField] AScoreCanvas m_Canvas;

	public async void FetchLeaderboard(Gameplay.Level level) {
		try {
			var entries = await Backend.GetScoresByLevel(level);
			m_Canvas.PopoulateEntries(entries.OrderBy(x => x.Score).ToList());
		}
		catch(Exception e) {
			Debug.LogError(e);
		}
	}

	public void OnCanvasClose() {
		OnCanvasClosed?.Invoke();
	}

	public void OnSubmit(string username) {
		OnSubmitted?.Invoke(username);
	}
}
