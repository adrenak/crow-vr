﻿using System;
using UnityEngine;

public class GameplayController : MonoBehaviour {
	public event Action<int> OnAllRocksCollected;
	public event Action OnTimeOver;
	public event Action OnGameStop;

	public Gameplay.Options CurrentOptions { get; private set; }
    public Gameplay.Level CurrentLevel { get; private set; }
	public Timer Timer { get { return m_Timer; } }

	[SerializeField] AGameplayCanvas m_Canvas;
	[SerializeField] DeviceController m_DeviceController;
	[SerializeField] RockInstantiater m_RockInstantiater;
	[SerializeField] Bottle m_Bottle;
	[SerializeField] Crow m_Crow;
	[SerializeField] Timer m_Timer;

	int m_CurrentScore;

	void Start() {
        m_Bottle.OnRockCollected += (count, total) => {
            m_CurrentScore = count;
			m_Canvas.SetRockCount(count, total);
        };

		m_Bottle.OnAllRocksCollected += count => {
			m_Crow.SetDrinkState(true);
			m_Timer.Pause();
			m_Canvas.SetRockCount(count, count);
			OnAllRocksCollected?.Invoke(count);
		};

		m_Timer.OnTimeOver += () => OnTimeOver();
    }

    public void OnApplicationStart() {
		m_Crow.SetDrinkState(false);
	}

	public void StartGame(Gameplay.Options options) {
        CurrentOptions = options;

		m_DeviceController.ActivateInteractors(DeviceController.InputMode.Both);

		m_RockInstantiater.maxPitch = options.maxPitch;
		m_RockInstantiater.minPitch = options.minPitch;
		m_RockInstantiater.maxYaw = options.maxYaw;
		m_RockInstantiater.minYaw = options.minYaw;
		m_RockInstantiater.InstantiateRocks(options.rockCount);

		m_Crow.SetDrinkState(false);
		m_Bottle.InitBottle(options.rockCount);

		if(options.HasTimeLimit)
			m_Timer.Run(options.timeLimit);
	}

    public void StopGame() {
		m_Timer.Pause();
		m_Crow.SetDrinkState(false);
		m_Bottle.ResetBottle();
		m_RockInstantiater.Clear();
		OnGameStop?.Invoke();
	}

	public int GetScore() {
		return m_CurrentScore;
	}

	public int GetPoints() {
		return m_Timer.GetTimeElapsed();
	}

	public int GetTimeLeft() {
		return m_Timer.GetTimeLeft();
	}

	public Gameplay.Level GetLevel() {
		return CurrentLevel;
	}

	public Gameplay.Options GetOptions() {
		return CurrentOptions;
	}
}
