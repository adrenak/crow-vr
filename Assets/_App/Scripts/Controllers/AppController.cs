﻿using UnityEngine;
using System.Collections;
using UnityEngine.XR;
using System;

public class AppController : MonoBehaviour {
    [SerializeField] AppUI m_AppUI;
	[SerializeField] GameplayController m_Gameplay;
	[SerializeField] MenuController m_Menu;
	[SerializeField] ScoreController m_Score;

	void Start() {
		Backend.Init();
		StartCoroutine(SetQuality());

		m_AppUI.ShowMenu();
		m_Gameplay.OnApplicationStart();
		m_Menu.OnApplicationStart();

		// Listen for app lifecycle events
		m_Menu.OnLevelSelect += level => StartGame(level);

		m_Gameplay.OnGameStop += StopGame;
		m_Gameplay.OnAllRocksCollected += OnAllRocksCollected;
		m_Gameplay.OnTimeOver += OnTimeOver;

		m_Score.OnSubmitted += username => SubmitScore(username);
		m_Score.OnCanvasClosed += ScoreSubmissionCancelled;
	}   

	async void SubmitScore(string username) {
		var level = m_Gameplay.CurrentLevel;
		var points = m_Gameplay.GetPoints();

		await Backend.Submit(username, level, points);

		m_AppUI.ShowMenu("Score submitted! Play again?");
	}
	
	void ScoreSubmissionCancelled() {
		m_AppUI.ShowMenu();
	}

	void StartGame(Gameplay.Level level) {
		var options = Gameplay.Options.GetPreset(level);

		m_AppUI.ShowGameplay(options.rockCount, options.HasTimeLimit);
		m_Gameplay.StartGame(options);
	}

	void StopGame() {
		m_AppUI.ShowMenu();
	}

	void OnAllRocksCollected(int count) {
        var level = m_Gameplay.GetLevel();
        var points = m_Gameplay.GetPoints();

        m_AppUI.ShowVictory(level, points);
		m_Score.FetchLeaderboard(m_Gameplay.CurrentLevel);
	}

	void OnTimeOver() {
		m_AppUI.ShowLoss();
	}

    IEnumerator SetQuality() {
        // Changing quality settings immediately after start 
        // does not seem to work. So we wait for a second
        // Set the AA and increase resolution upsampling
        yield return new WaitForSeconds(1);
        QualitySettings.antiAliasing = 6;
        XRSettings.eyeTextureResolutionScale = 1.3f;
    }
}
