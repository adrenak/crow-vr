﻿using PlayerIOClient;
using System;
using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;

public static class Backend {
    public static Client Client { get; private set; }

    public static void Init() {
        PlayerIO.Authenticate(
            "crow-vr-qlhrkfnqvee3yx2zuhbtbq",
            "public",
            new Dictionary<string, string> {
                    { "username", "admin" },
                    { "password", "admin" }
            }
            , null,
            client => {
                Client = client;
                Debug.Log(client.ConnectUserId);
            },
            error => {
                Debug.Log(error.Message);
            }
        );
    }

    public static UniTask Submit(string username, Gameplay.Level level, int score) {
        var source = new UniTaskCompletionSource();

        Submit(username, level, score,
            () => {
                source.TrySetResult();
            },
            onFail => {
                source.TrySetException(onFail);
            }
            );

        return source.Task;
    }

    static void Submit(string username, Gameplay.Level level, int score, Action onSuccess, Action<Exception> onFailure) {
        Client.BigDB.LoadOrCreate("Scores", System.Guid.NewGuid().ToString(),
            res => {
                res.Set("name", username);
                res.Set("score", score);
                res.Set("level", level.ToString());

                res.Save(
                        () => onSuccess?.Invoke(),
                            error => onFailure?.Invoke(new Exception(error.ErrorCode + ": " + error.Message))
                        );
            });
    }

    public static UniTask<List<UserScoreEntry>> GetScoresByName(string username) {
        var source = new UniTaskCompletionSource<List<UserScoreEntry>>();
        GetScoresByName(username,
            res => {
                source.TrySetResult(res);
            },
            onFail => {
                source.TrySetException(onFail);
            });
        return source.Task;
    }

    static void GetScoresByName(string username, Callback<List<UserScoreEntry>> entries, Action<Exception> onFailure) {
        List<UserScoreEntry> m_entries = new List<UserScoreEntry>();
        Client.BigDB.LoadRange("Scores", "byname", new object[] { username }, null, null, 10,
            res => {
                Debug.Log(res.Length);
                if (res.Length > 0) {
                    foreach (var r in res) {
                        m_entries.Add(new UserScoreEntry {
                            Name = r.GetString("name"),
                            Score = r.GetInt("score"),
                            Level = (Gameplay.Level)Enum.Parse(typeof(Gameplay.Level), r.GetString("level"))
                        });
                        entries(m_entries);
                    }
                }
                else {
                    onFailure?.Invoke(new Exception("Retrived db length is 0"));
                }
            });
    }

    public static UniTask<List<UserScoreEntry>> GetScoresByLevel(Gameplay.Level level) {
        var source = new UniTaskCompletionSource<List<UserScoreEntry>>();
        GetScoresByLevel(level,
            res => source.TrySetResult(res),
            onFail => source.TrySetException(onFail)
        );
        return source.Task;
    }

    static void GetScoresByLevel(Gameplay.Level level, Callback<List<UserScoreEntry>> entries, Action<Exception> onFailure) {
        List<UserScoreEntry> m_entries = new List<UserScoreEntry>();
        Client.BigDB.LoadRange("Scores", "bylevel", new object[] { level.ToString() }, null, null, 10,
            res => {
                Gameplay.Level dlevel;
                Debug.Log(res.Length);
                if (res.Length > 0) {
                    foreach (var r in res) {
                        m_entries.Add(new UserScoreEntry {
                            Name = r.GetString("name"),
                            Score = r.GetInt("score"),
                            Level = Enum.TryParse(r.GetString("level"), out dlevel) ? dlevel : Gameplay.Level.Easy
                        });
                    }
                    entries(m_entries);
                }
                else {
                    onFailure?.Invoke(new Exception("Retrived db length is 0"));
                }
            });
    }

    public static UniTask<List<UserScoreEntry>> GetScoresByNameAndLevel(string username, Gameplay.Level level) {
        var source = new UniTaskCompletionSource<List<UserScoreEntry>>();
        GetScoresByNameAndLevel(username, level,
            res => {
                source.TrySetResult(res);
            },
            onFail => {
                source.TrySetException(onFail);
            });
        return source.Task;
    }

    static void GetScoresByNameAndLevel(string username, Gameplay.Level level, Callback<List<UserScoreEntry>> onSuccess, Action<Exception> onFailure) {
        List<UserScoreEntry> m_Entries = new List<UserScoreEntry>();
        Client.BigDB.LoadRange("Scores", "bynameandlevel", new object[] { username, level.ToString() }, null, null, 10,
            res => {
                if (res.Length == 0) {
                    onFailure?.Invoke(new Exception("Retrived db length is 0"));
                    return;
                }

                foreach (var r in res) {
                    var entry = new UserScoreEntry {
                        Name = r.GetString("name"),
                        Score = r.GetInt("score"),
                        Level = (Gameplay.Level)Enum.Parse(typeof(Gameplay.Level), r.GetString("level"))
                    };
                    Debug.Log(entry.ToString());
                    m_Entries.Add(entry);
                }
                onSuccess?.Invoke(m_Entries);
            });
    }
}

public class UserScoreEntry {
    public string Name;
    public Gameplay.Level Level;
    public int Score;

    public override string ToString() {
        return Name + " " + Level + " " + Score;
    }
}
