﻿using UnityEngine;
using System.Collections.Generic;
using Adrenak.UniGenVR; 

public class RockInstantiater : MonoBehaviour {
    [Range(0, 5)]
    public float distance = 5;

    [Range(-80, 0)]
    public float minYaw = -80;

    [Range(0, 80)]
    public float maxYaw = 80;

    [Range(-20, 0)]
    public float minPitch = -80;

    [Range(0, 80)]
    public float maxPitch = 80;

    [SerializeField] GameObject m_RockPrefab;
    [SerializeField] Transform m_RockParent;
	[SerializeField] Transform m_Pivot;

    public List<GameObject> rocks = new List<GameObject>();

    public void InstantiateRocks(int rockCount) {
        for (int i = 0; i < rockCount; i++) {
            float[] angles = GetRandomYawAndPitch();
             InstantiateRock(angles[0], angles[1]);
        }
    }

	public void Clear() {
		foreach (var rock in rocks)
			if (rock != null)
				Destroy(rock.gameObject);
	}

    float[] GetRandomYawAndPitch() {
        var pitchAngle = Random.Range(minPitch, maxPitch);
		float yawAngle = 0;
        while(yawAngle < 15 && yawAngle > -15) 
            yawAngle = Random.Range(minYaw, maxYaw);
        return new float[] { yawAngle, pitchAngle };
    }

    void InstantiateRock(float yaw, float pitch) {

        float y = Mathf.Sin(Mathf.Deg2Rad * pitch) * distance;
        float planeRadius = Mathf.Cos(Mathf.Deg2Rad * pitch) * distance;

        float x = Mathf.Sin(Mathf.Deg2Rad * yaw) * planeRadius;
        float z = Mathf.Cos(Mathf.Deg2Rad * yaw) * planeRadius;

		var position = m_Pivot.position + new Vector3(x, y, -z);
		var go = Instantiate(m_RockPrefab, position, Quaternion.Euler(Random.Range(0,180), Random.Range(0, 180), Random.Range(0, 180)), this.transform);
        rocks.Add(go);
    }
    
    public void DestroyRocks() {
        foreach(var r in rocks) {
            Destroy(r);
        }
    }
}
