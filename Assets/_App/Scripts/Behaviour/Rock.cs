﻿using UnityEngine;
using Adrenak.UniGenVR;

public class Rock : MonoBehaviour {
    private Interactable m_Interactable;
    private Interactor m_Interactor;

    private MeshRenderer m_Renderer;
    Vector3 m_Destination;

    private void Start() {
        m_Interactable = GetComponent<Interactable>();
        m_Renderer = GetComponent<MeshRenderer>();

        m_Interactable.OnBeingGazedEvent += OnBeingGazedEvent;
        m_Interactable.OnGazedEvent += HandleOnGazedOver;

		m_Renderer.material.color = Color.red;
    }

    private void OnBeingGazedEvent(float obj) {
        var color = Color.Lerp(Color.red, Color.green, obj);
        m_Renderer.material.color = color;
    }

    void Update() {
        if (m_Interactor == null) return;

		m_Destination = m_Interactor.transform.position + m_Interactor.GetRay().direction * 5f;
		transform.position = Vector3.Lerp(transform.position, m_Destination, 5 * Time.deltaTime);
    }

    private void HandleOnGazedOver(Interactor interactor) {
        m_Interactable.enabled = false;
		m_Interactor = interactor;
        GetComponent<SimpleFloater>().enabled = false;
    }

    public void OnHitBottle() {
        Destroy(gameObject);
    }
}
