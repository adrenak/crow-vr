﻿using UnityEngine;

public class Crow : MonoBehaviour {
	[Header("Movement")]
    [SerializeField] float m_Speed;
	[SerializeField] Transform m_DrinkingPosition;
    [SerializeField] Transform m_SittingDestination;

	[Header("Effects")]
	[SerializeField] AnimalController m_AnimController;
	[SerializeField] AudioSource m_AudioSource;

    Transform m_CrowDestination;
    bool m_CanDrink;

	private void Start() {
		m_AnimController.OnStateChange += newState => {
			if(newState == AnimalController.State.Moving)
				m_AudioSource.Play();
			else
				m_AudioSource.Stop();
		};

		m_CrowDestination = m_SittingDestination;
	}

	public void SetDrinkState(bool canDrink) {
        m_CanDrink = canDrink;
		m_CrowDestination = m_CanDrink ? m_DrinkingPosition : m_SittingDestination;
    }

    void Update() {
        float step = m_Speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, m_CrowDestination.position, step);

		if (Vector3.Distance(transform.position, m_CrowDestination.position) < .2f) {
			transform.eulerAngles = m_CrowDestination.eulerAngles;
			if(m_CrowDestination == m_SittingDestination)
				m_AnimController.ChangeState(AnimalController.State.Idle);
			else if (m_CrowDestination == m_DrinkingPosition)
				m_AnimController.ChangeState(AnimalController.State.Doing);
		}
		else {
			m_AnimController.ChangeState(AnimalController.State.Moving);
			transform.LookAt(m_CrowDestination);
		}
    }
}
