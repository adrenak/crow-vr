﻿using UnityEngine;

public class Water : MonoBehaviour {
	[SerializeField] float m_InitialFill = .2f;

	float m_DestinationScale;
    Vector3 m_InitialScale;

	private void Start() {
		ResetWater();
        m_InitialScale = transform.localScale;
	}

	[ContextMenu("Test")]
	void Test() {
		FillWater(.50f);
	}

	public void FillWater(float percentage) {
		m_DestinationScale = m_InitialFill + (1 - m_InitialFill) * percentage;
    }

	private void Update() {
		transform.localScale = Vector3.Slerp(
			transform.localScale,
			new Vector3 {
				x = m_InitialScale.x,
				y = m_DestinationScale,
				z = m_InitialScale.z
			},
			Time.deltaTime * 5
		);
	}

	public void ResetWater() {
		m_DestinationScale = m_InitialFill;
    }
}
