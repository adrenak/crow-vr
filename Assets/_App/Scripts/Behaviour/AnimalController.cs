﻿using System;
using UnityEngine;

public class AnimalController : MonoBehaviour {
	[Serializable]
	public enum State {
		Idle,
		Moving,
		Doing
	}

	public Animator m_Animator;
	public event Action<State> OnStateChange;
	public State InnerState;

	public void ChangeState(State s) {
		if (InnerState == s) return;

		InnerState = s;

		switch (s) {
			case State.Idle:
				m_Animator.SetBool("isIdle", true);
				m_Animator.SetBool("isMoving", false);
				m_Animator.SetBool("isDoing", false);
				break;

			case State.Moving:
				m_Animator.SetBool("isIdle", false);
				m_Animator.SetBool("isMoving", true);
				m_Animator.SetBool("isDoing", false);
				break;

			case State.Doing:
				m_Animator.SetBool("isIdle", false);
				m_Animator.SetBool("isMoving", false);
				m_Animator.SetBool("isDoing", true);
				break;
		}
		OnStateChange?.Invoke(InnerState);
	}

	[ContextMenu("idle")]
	public void TestIdle() {
		ChangeState(State.Idle);
	}

	[ContextMenu("moving")]
	public void TestMoving() {
		ChangeState(State.Moving);
	}

	[ContextMenu("doing")]
	public void TestDoing() {
		ChangeState(State.Doing);
	}
}
