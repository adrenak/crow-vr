﻿using UnityEngine;

public class SimpleFloater : MonoBehaviour {
    public float scale;
    Vector3 initialPos;
    float seed;

    private void Start() {
        initialPos = transform.position;
        seed = Random.value;
    }

    private void Update() {
        var displacement = new Vector3(
            Mathf.PerlinNoise(.5f + seed, Time.time / 3),
            Mathf.PerlinNoise(.5f + seed, Time.time / 3),
            Mathf.PerlinNoise(.5f + seed, Time.time / 3)
        );

        transform.position = initialPos + displacement / scale;
    }
}
