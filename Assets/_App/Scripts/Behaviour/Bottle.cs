﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Adrenak.UniGenVR;

public class Bottle : MonoBehaviour {
	[SerializeField] AudioSource m_AudioSource;
	[SerializeField] Water m_Water;
	[SerializeField] GameObject m_MockRock;
	[SerializeField] Transform m_MockRockPoint;
    [SerializeField] int m_RockCapacity;
    [SerializeField] int m_RockCount;

    public Action<int, int> OnRockCollected;
    public Action<int> OnAllRocksCollected;

	public Water InnerWater { get { return m_Water; } }

	List<GameObject> mockRocks = new List<GameObject>();

    private void Start() {
		InnerWater.ResetWater();
		ClearRocks();
	}

	public void InitBottle(int rockCapacity) {
        m_RockCapacity = rockCapacity;
        m_RockCount = 0;
		ClearRocks();
		InnerWater.ResetWater();
	}

	public void ResetBottle() {
		ClearRocks();
		InnerWater.ResetWater();
	}

	public void ClearRocks() {
		foreach (var rock in mockRocks)
			Destroy(rock);
	}

    private void OnTriggerEnter(Collider other) {
        var go = other.gameObject;
        var rock = go.GetComponent<Rock>();

        if (rock != null) {
			m_AudioSource.pitch = UnityEngine.Random.Range(.85f, 1.05f);
			m_AudioSource.Play();

            rock.OnHitBottle();
            m_RockCount++;
            if (m_RockCount >= m_RockCapacity) 
                OnAllRocksCollected?.Invoke(m_RockCapacity);
			else
				OnRockCollected?.Invoke(m_RockCount, m_RockCapacity);

			float percentage = (float)m_RockCount / m_RockCapacity;
			InnerWater.FillWater(percentage);

			var mockRock = Instantiate(m_MockRock, m_MockRockPoint.position, Quaternion.identity);
			mockRock.transform.rotation = UnityEngine.Random.rotation;
			mockRocks.Add(mockRock);
		}
    }
}
